


$(function(){
  // se declara de forma gloval la modalidad para su facilidad de uso
  var modalidad="";
  var primero='';
  var data = [
        {
            "hc-key": "mx-3622",
            "value": 0
        },
        {
            "hc-key": "mx-bc",
            "value": 1
        },
        {
            "hc-key": "mx-bs",
            "value": 2
        },
        {
            "hc-key": "mx-so",
            "value": 3
        },
        {
            "hc-key": "mx-cl",
            "value": 4
        },
        {
            "hc-key": "mx-na",
            "value": 5
        },
        {
            "hc-key": "mx-cm",
            "value": 6
        },
        {
            "hc-key": "mx-qr",
            "value": 7
        },
        {
            "hc-key": "mx-mx",
            "value": 8
        },
        {
            "hc-key": "mx-mo",
            "value": 9
        },
        {
            "hc-key": "mx-df",
            "value": 10
        },
        {
            "hc-key": "mx-qt",
            "value": 11
        },
        {
            "hc-key": "mx-tb",
            "value": 12
        },
        {
            "hc-key": "mx-cs",
            "value": 13
        },
        {
            "hc-key": "mx-nl",
            "value": 14
        },
        {
            "hc-key": "mx-si",
            "value": 15
        },
        {
            "hc-key": "mx-ch",
            "value": 16
        },
        {
            "hc-key": "mx-ve",
            "value": 17
        },
        {
            "hc-key": "mx-za",
            "value": 18
        },
        {
            "hc-key": "mx-ag",
            "value": 19
        },
        {
            "hc-key": "mx-ja",
            "value": 20
        },
        {
            "hc-key": "mx-mi",
            "value": 21
        },
        {
            "hc-key": "mx-oa",
            "value": 22
        },
        {
            "hc-key": "mx-pu",
            "value": 23
        },
        {
            "hc-key": "mx-gr",
            "value": 24
        },
        {
            "hc-key": "mx-tl",
            "value": 25
        },
        {
            "hc-key": "mx-tm",
            "value": 26
        },
        {
            "hc-key": "mx-co",
            "value": 27
        },
        {
            "hc-key": "mx-yu",
            "value": 28
        },
        {
            "hc-key": "mx-dg",
            "value": 29
        },
        {
            "hc-key": "mx-gj",
            "value": 30
        },
        {
            "hc-key": "mx-sl",
            "value": 31
        },
        {
            "hc-key": "mx-hg",
            "value": 32
        }
    ];


  $(document).ready(function () {
      cargar_mapa();

     estados_gloval=[];


          //busca los estados
          busca_estados()

         //carga los datos en la tabla
         mostrar_seleccion(primero);
        // var tipo=document.getElementsByName("tipo")[0].value;




   });


//********************************************************************************************************
function  cargar_mapa(){

console.log(data);
  $('#container').highcharts('Map', {

        title : {
            text : 'Highmaps basic demo'
        },

        subtitle : {
            text : 'Source map: <a href="https://code.highcharts.com/mapdata/countries/mx/mx-all.js">Mexico</a>'
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series : [{
            data : data,
            mapData: Highcharts.maps['countries/mx/mx-all'],
            joinBy: 'hc-key',
            name: 'Random data',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }]
    });

}




   //se buscan los  estados de mexico y los almacena en un array
   ///***************************************************************************************************************************

   function busca_estados(){
console.log('buscar estados');
             $.getJSON('http://oicc.96.lt/mapa/busqueda_estado.php',function(data){

                           for (var i = 0; i < data.length; i++) {

                                  estados_gloval.push(data[i].entidad);
                           }


                             console.log(estados_gloval);
                             //carga los  estadoe en el select
                             busca_modalidad();


             });

      }
   //***************************************************************************************************************************************
   function busca_modalidad(){
console.log('buscar modalidad------------------------------------');

                           $.getJSON('http://oicc.96.lt/mapa/Incidencia_delictiva.php',function(data){


                                         var datos;
                                         var slct;
                                         for (var i = 0; i < data.length; i++) {
                                           if (i==0) {
                                             // este le da el valor a la modalidad en su parte inicial
                                             modalidad =data[i].modalidad;
                                             tr = $('<option id="option_est"  value="'+data[i].ID_modalidad+'"/>');
                                                   tr.append(data[i].modalidad);

                                                 $('#modalidad').append(tr);

                                           }
                                           else{

                                             tr = $('<option id="option_est"  value="'+data[i].ID_modalidad+'"/>');
                                                   tr.append(data[i].modalidad);

                                                 $('#modalidad').append(tr);


                                           }


                                         }
                                         //carga los estado em el select
                                         busca_tipo(1);
                                        // mostrar_seleccion(document.getElementsByName("tipo")[0].value);


                           });



      }
      //****************************************************************************************************************************

//metodo para cargar los municipios en el select  de municipios el cual pide como parametro el estado
   function busca_tipo(ID_modalidad){

                                       var funcion_num=1;//esta variable da referencia a que funcion  se mandara a llamar


                                       remove_tipo ()
                                  // se convierten a mayusculas para  realizar la consulta y  descartar posible error de munuscula

                                       $.ajax({


                                           type: "POST",
                                           dataType: "json",
                                           url:"http://oicc.96.lt/mapa/Incidencia_delictiva.php" ,
                                           type:"POST",
                                           data:{
                                               funcion_num:funcion_num,
                                               ID_modalidad:ID_modalidad
                                           }

                                       }).done(function(data){
                                         //despues de que se realiza la consulta se obtiene los datos en un objeto  el cual tiene los municipios

                                             for (var i = 0; i < data.length+1; i++) {
                                               if (i==0) {
                                                  primero='TODOS';

                                               }
                                               if (i==0) {
                                                         tr = $('<option/>');
                                                           tr.append('TODOS');


                                                         $('#kirby').append(tr)

                                                        }

                                            else{

                                                      tr = $('<option/>');
                                                        tr.append(data[i-1].subtipo);


                                                      $('#kirby').append(tr);

                                                }

                                             }


                                             mostrar_seleccion(primero);


                                       });



   }
   //******************************************************************************************************************************************
   //elimina todo los elementos cargados anteriormente en el select de municipios
   function remove_tipo () {
     $('#kirby option[value!="0"]').remove();


   }


   //******************************************************************************************************************************************
   //elimina la tabla y todos lsus elemtos
   function remove_tabla () {

             var x = document.getElementById("tbody");

            $(x).remove();
            tr = $('<tbody id="tbody"/>');


            $('#table').append(tr);

   }
//**********************************************************************************************************************************************

   function mostrar_seleccion(tipo){
     console.log('buscar mostrar');

          remove_tabla();



// console.log(estados_gloval);
var esta2 = JSON.stringify(estados_gloval);
console.log(esta2+modalidad+tipo+'---------------------');

                 $.ajax({


                           type: "POST",
                           dataType: "json",
                           url:"http://oicc.96.lt/mapa/graficacion_delitos.php" ,

                           data:{
                             modalidad:modalidad,
                             tipo:tipo,
                             esta2:esta2,



                           }

                 }).done(function(data){
                   //despues de que se realiza la consulta se obtiene los datos en un objeto  el cual tiene los municipios

                       for (var i = 0; i < data.length; i++) {

                                      tr = $('<tr/>');
                                        tr.append(
                                                    "<td>"+ estados_gloval[i]+"</td>"+
                                                    "<td>"+ data[i].numero+"</td>"

                                                 );

                                      $('#tbody').append(tr);


                       }


                 })



           }
//***********************************************************************************************************************
//cuando se realice la elecccion del tipo
   $("#kirby").change(function(){

 //var tipo=document.getElementsByName("tipo")[0].value;
        tipo= $( "#kirby option:selected" ).text();
         alert(tipo);
          mostrar_seleccion(tipo);



      });
//cuande se realice la  selleccion del estado
//**********************************************************************************************************************
      $("#modalidad").change(function(){
          var ID_mod= $( "#modalidad" ).val();
          modalidad= $( "#modalidad option:selected" ).text();
          busca_tipo(ID_mod);



         });

         $("#municipios").change(function(){

       var municipio=document.getElementsByName("municipios")[0].value;
                mostrar_seleccion(municipio);



            });





});
